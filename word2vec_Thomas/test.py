#http://radimrehurek.com/2014/02/word2vec-tutorial/ gensim and word2vec
import gensim, logging, numpy as np
model = gensim.models.Word2Vec.load_word2vec_format('../../vectors.bin', binary=True)

text1 = "The Rotterdam Blitz was the aerial bombardment of Rotterdam by the Luftwaffe (German air force) on 14 May 1940, during the German invasion of the Netherlands in World War II. The objective was to support the German troops fighting in the city, break Dutch resistance and force the Dutch to surrender. Even though preceding negotiations resulted in a ceasefire, the bombardment took place nonetheless, in conditions which remain controversial, and destroyed almost the entire historic city centre, killing nearly nine hundred civilians and leaving 30,000 people homeless.[contradiction] \n\nThe psychological and physical success of the raid, from the German perspective, led the Oberkommando der Luftwaffe (OKL) to threaten to destroy the city of Utrecht if the Dutch Government did not surrender. The Dutch capitulated early the next morning."
text2 = "The Germans accepted the reply from Scharroo. General Schmidt had his interpreter quickly draw up a new letter, more extended than the first one, giving the Dutch until 16:20 to comply. He undersigned the new ultimatum with his name and rank. When Captain Backer was being escorted back by Oberstleutnant von Choltitzt to the Maas bridges, German bombers appeared from the south. General Schmidt, who was joined by the two Generals von Hubicki and Student, saw the planes and cried out My God, this is going to be a catastrophe![citation needed] Panic struck German soldiers on the Noordereiland, most of which were totally unaware of the events being played out between the top brass of bo"
text3= "th sides. They feared being attacked by their own bombers. Von Choltitz ordered red flares to be launched, and when the first three bombers overhead dropped their bombs the red flares were obscured by smoke. The next 24 bombers of the southern formation closed their bomb hatches and turned westwards. \n\nThe other much larger formation came from the northeast. It comprised 60 bombers under Oberst Lackner. Due to the dense smoke, the formation had"



N = 200
#create dicts to hold observations of each topic
dict1 = dict();
dict2 = dict();
for i in range(N):
    dict1[i] = set([])
    dict2[i] = set([])

topics1 = np.zeros((N,1))
threshold = 0.2
for word in text1.split(" "):
    if word in model:
        vec = model[word];
        for i, score in enumerate(vec):
            if score > threshold:
                #lets count this topic as present
                topics1[i] += 1
                dict1[i].add(word)

topics2 = np.zeros((N,1))
for word in text2.split(" "):
    key = "'" + word + "'"
    if word in model:
        vec = model[word];
        for i, score in enumerate(vec):
            if score > threshold:
                #lets count this topic as present
                topics2[i] += 1
                dict2[i].add(word)

print topics1
print topics2

for key in dict1:
    if dict1[key] != set([]):
        print "Dimension " + str(key)
        for item in dict1[key]:
            print str(item) + " " + str(model[item][key]),
        print '\n'

only1 = []
only2 = []
both = []
for key in dict1:
    if dict1[key] != set([]):
        if dict2[key] != set([]):
            both.append(key)
        else:
            only1.append(key)
    elif dict2[key] != set([]):
        only2.append(key)

print len(only1)
print len(only2)
print len(both)

print 'both'
for item in both:
    print dict1[item]
    print dict2[item]
    print '\n'

print 'only1'
for item in only1:
    print dict1[item]
    print '\n'

print 'only2'
for item in only2:
    print dict2[item]
    print '\n'

from time import sleep

print model

#trying to represent topics: build inverted index on vectors to words
print 'representing topics'
inverted = dict();
for i in range(dimensions):
    inverted[key] = set([])

threshold2 = 0.2
#for all words in the model; add
for word in model:
    print word
    #for each dimension, if represented at least 0.2 we'll count it for the dim
#TODO test this value
    for i, value in enumerate(model[word]):
        if value > threshold2:
            inverted[i].add(word)
            print value
    sleep(0.1)

#TODO cluster
kmeans = dict()
for dimension in range(dimensions):
    kmeans[dimension] = []
    #find clusters, add to this dimension

#now print all dimensions all words
for dimension in range(dimensions):
    print 'dimension ' + str(dimension)
    for word in inverted[dimension]:
        print word
    print 'represented by:'
    for word in kmeans[dimension]:
        print word
    sleep(1000)
