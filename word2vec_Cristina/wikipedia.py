#training for wikipedia word2vec embeddings
#TODO: library function parses the xml and then gives an array of words per
#documents rather than on sentence level (without giving the original sentence
#breaks/periods). does this affect training? sounds like it might lose context

from gensim.corpora import WikiCorpus
from gensim.models.word2vec import Word2Vec
from time import sleep
import logging
from nltk import tokenize
from nltk.stem.snowball import DutchStemmer

stemmer = DutchStemmer()

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
corpus = WikiCorpus('nlwiki-latest-pages-articles.xml.bz2',dictionary=False)
max_sentence = -1

#big dictionary containing all...
big_dic = []
#def generate_lines():
for index, text in enumerate(corpus.get_texts()):
    if index < max_sentence or max_sentence==-1:
#TODO attempt at the todo, but doesn't work as punctuation was already filtered,
#making this attempt to tokenise in sentence level pointless
#            print " ".join(text)
#            sentences = tokenize.sent_tokenize(text)
#            for sent in sentences:
#                print sent
#                sleep(3)

        big_dic.append([stemmer.stem(x.decode('utf8')).lower() for x in text])
#        yield text
    else:
        break

model = Word2Vec(big_dic)

#model = Word2Vec() 
#model.build_vocab(generate_lines())
#model.train(generate_lines(),chunksize=500)

# pickle the entire model to disk, so we can load&resume training later
model.save('./wiki.model')
# store the learned weights, in a format the original C tool understands
model.save_word2vec_format('./wiki.model.bin', binary=True)
