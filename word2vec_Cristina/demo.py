import sys
from time import sleep
import numpy
from scipy import linalg, mat, dot

arg = 'Rotterdam'

if(len(sys.argv) > 1):
    arg = sys.argv[1]

from gensim.models.word2vec import Word2Vec
model = Word2Vec.load_word2vec_format('./ldj.withreg.model.bin', binary=True)

print '+vrouw +man -zoon'
print model.most_similar(positive=['man', 'vrouw'], negative=['zoon'])
print '\n\n'
vrouw = model['vrouw']
man = model['man']
zoon = model['zoon']

dochter = (vrouw - zoon).T

bestword = ''
vec = man
simi = 0
count = 0
for word in model.vocab:
    count += 1
    propsimi = dot(model[word],dochter)/linalg.norm(model[word])/linalg.norm(dochter.T)
    if word == "ontroerd":
        print 'volgende'
        print propsimi
    if propsimi > simi: #model[word]
        vec = model[word]
        simi = propsimi
        print simi
        bestword = word

print count
print 'best word is ' + str(bestword) + ' with similarity ' + str(simi)
print '\n\n'
print 'most similar to arg ' + str(arg)
print model.most_similar(positive=[arg])
