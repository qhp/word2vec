#TODO
#more normalisation on words
import gensim
from gensim.corpora import WikiCorpus
from gensim.models.word2vec import Word2Vec
from lxml import etree
from time import sleep
import os
import xml.etree.ElementTree as ET
import zipfile, gzip
import xml.etree.cElementTree as ElementTree
import HTMLParser
import sys
import logging
from nltk import tokenize

#additional normalisation..
from nltk.stem.snowball import DutchStemmer

stemmer = DutchStemmer()

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
corpus = WikiCorpus('nlwiki-latest-pages-articles.xml.bz2',dictionary=False)
max_sentence = -1


input_dir = './data/'

#our 'dictionary': a list of sentences, where each sentence consists of a list
#of words
big_dic = []
def generate_lines():

    for filename in os.listdir(input_dir):
        zipfilename = input_dir + filename
        print "Processing:",zipfilename

        filename = zipfilename.split("/")[-1]
        logfile = './progress/' + filename.replace('.gz', '.log')
        print "Checking progress file %s" % logfile
        if os.path.exists(logfile):
            print "[%s] File already imported." % logfile
        try:
            with gzip.GzipFile(zipfilename) as file:
                txt = file.read()
                txt = txt.decode('ascii', 'ignore') #read(utf) decode utf8
                txt = txt.replace('&','&amp;') # quick and dirty solution to encoding entities
                #etree[0] = docinfo, etree[1] = meta etc
                #etree[0].tag = docinfo, etree[0].attrib is dict with attributes
#                etree = ElementTree.fromstring(txt)
                #sleep(10)
                #print ET.tostring(etree, encoding='ascii', method='text')

                sents = tokenize.sent_tokenize(txt) #the first 2 are authors & stuff
#nice sentence for tests                print sents[98]

                #1. word_tokenize for tokenisatoin
                #2. remove caps
                #3. dutchstemmer
                #4. dutch stopwords nltk.corpus.stopwords.words('dutch')
                #   could be removed, but we leave them in during training
                #   for context..
                for sent in sents:
                    big_dic.append([stemmer.stem(x).lower() for x in tokenize.word_tokenize(sent)])
                #tree = etree.parse(etree)
                #yield tokenize.sent_tokenize(ET.tostring(etree, encoding='utf8', method='text'))

        except Exception as error:
            print type(error), "in", zipfilename + ":", error
            import traceback
            traceback.print_exc()

#def generate_lines():
#    for index, text in enumerate(corpus.get_texts()):
#        if index < max_sentence or max_sentence==-1:
#            yield tokenize.sent_tokenize(text)
#        else:
#            break  

#populate big_dic
generate_lines()

model = Word2Vec(big_dic)
#model = Word2Vec() 
#model.build_vocab(big_dic)
#model.train(generate_lines(),chunksize=500)

print '\n\n\n'
print model
model.save('./ldj.norm.withreg.model')

#test functionality
print 'Most similar to Rotterdam'
print model.most_similar(positive=['rotterdam'])

# pickle the entire model to disk, so we can load&resume training later
model.save('./text8.ldj.model')
print 'storing binary.. \n\n'


# store the learned weights, in a format the original C tool understands
model.save_word2vec_format('./model.ldj.text8.bin', binary=True)
